﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace Plataforma
{
    public class Carrinho:List<Model.Carrinho.Item>
    {
        

        private Carrinho()
        {

        }

        public static Carrinho IniciarVenda()
        {
            return new Carrinho();
        }
    }
}
